interface Odd {
  [key: string]: number;
}

export default interface IOrder {
  choose: string;
  wars: number;
  odds: Odd;
  bet_gold: number;
}
