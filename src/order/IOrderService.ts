import IOrder from './IOrder';

export default interface IOrderService {
  game_name: string;
  game_num: string;
  currency: string;
  orders: IOrder[];
}
