import IOrderService from './IOrderService'
import IOrder from './IOrder'

interface Status {
  status: string
}

export default class OrderService implements IOrderService {
  game_name: string = ''
  game_num: string = ''
  currency: string = ''
  orders: IOrder[] = []

  constructor(game_name: string) {
    this.game_name = game_name
  }

  bet(bet: IOrderService): Status {
    return {
      status: 'success'
    }
  }
}